public class JavaServer{

    public static void main(String[] args) throws Exception{
        //开启一个服务端Socket，监听12345端口
        ServerSocket server = new ServerSocket(12345);

        //有客户端连进来
        Socket client = server.accept();

        //获取到客户端输入流
        InputStream in = client.getInputStream();

        //准备一个缓冲数组
        byte data[]=new byte[4096];

        //处理读取的数据
        in.read(data);
        String s = new String(data);
        String[] s1 = s.split(" ");
        String param = s1[1].substring(1);
        String[] split = param.split("=");
        String[] split1 = split[1].split("&");
        int a = Integer.parseInt(split1[0]);
        int b = Integer.parseInt(split[2]);
        int result = 0;
        if (param.startsWith("add")){
            result = a+b;
        }
        if (param.startsWith("mult")){
            result = a*b;
        }

        //制作响应报文
        StringBuffer response = new StringBuffer();

        //响应状态
        response.append("HTTP/1.1 200 OK\r\n");

        //响应头
        response.append("Content-type:text/html\r\n\r\n");

        //要返回的内容(当前时间)
        response.append("result:"+result);


        //获取客户端的输出流
        OutputStream out=client.getOutputStream();

        //将以上内容写入
        out.write(response.toString().getBytes());

        //关闭客户端和服务端的流和Socket
        out.close();
        in.close();
        client.close();
        server.close();
    }
}

